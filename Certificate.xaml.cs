﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SD2CW1
{
    /// <summary>
    /// Author: Alex Dan
    /// Close button closes the window.
    /// Displays the certificate of attendance depending on the role of the atendee.
    /// Last modified: 2016-10-17
    /// </summary>
    public partial class Certificate : Window
    {
        Atendee atendee1 = new Atendee();
        public Certificate(Atendee a)
        {
            InitializeComponent();
            atendee1 = a;
            //Checks if the institution name is set
            if (atendee1.InstName.Length > 0)
            {
                lblText1.Content = "This is to certify that " + atendee1.FirstName + " " + atendee1.LastName + " attended " + atendee1.ConfName ;
                //Checks if the atendee is a presenter
                if (atendee1.Presenter)
                    lblText2.Content = " and presented the paper entitled " + atendee1.PaperTitle;            
            }
        }
        private void btnClose_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
