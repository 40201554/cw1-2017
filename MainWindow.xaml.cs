﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SD2CW1
{
    /// <summary>
    /// Author: Alex Dan
    /// Buttons: 
    ///     Set:Takes the values contained in the text boxes and updates the Attendee class properties
    ///     Clear:Removes the contents of the text boxes(does not change any properties of the class
    ///     Get:Uses the values of the Attendee class methods to update the text boxes.
    ///     Invoice: Opens a new window that represents an invoice.The window displays the name and institution of the Attendee along with the 
    /// conference name and the price to be paid as determined by the getCost() method.
    ///     Certificate: Opens a new window that represents a certificate of attendance.
    ///  Date last modified : 2016-10-18
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }
        //atendee1 is the object where all the data input by the user in the GUI is stored
        Atendee atendee1 = new Atendee();    
        private void btnClear_Click(object sender, RoutedEventArgs e)
        {
            //Every text box is set to blank and radio buttons were set to the initial value in the GUI when the CLEAR button is pressed
            txtAtendeeRef.Text = "";
            txtConfName.Text = "";
            txtFirstName.Text = "";
            txtInstName.Text = "";
            txtLastName.Text = "";
            radPaidYes.IsChecked = true;
            txtPaperTitle.Text = "";
            radPresenterYes.IsChecked = true;
            CmbRegType.SelectedIndex = -1;

        }

        private void btnSet_Click(object sender, RoutedEventArgs e)
        {
            // This  try/catch displays a error message if the validation required is not met, otherwise the variables will be set to 
            //the variables input by the user. 
            try
            {
                atendee1.FirstName = txtFirstName.Text;
                atendee1.LastName = txtLastName.Text;
                atendee1.AtendeeRef = Int32.Parse(txtAtendeeRef.Text);
                atendee1.ConfName = txtConfName.Text;
                if (radPaidYes.IsChecked == true)
                    atendee1.Paid = true;
                else
                    atendee1.Paid = false;
                if (radPresenterYes.IsChecked == true)
                    atendee1.Presenter = true;
                else
                    atendee1.Presenter = false;
                atendee1.RegType = CmbRegType.Text;
                atendee1.InstName = txtInstName.Text;
                atendee1.PaperTitle = txtPaperTitle.Text;
            }
            catch (Exception ex)
            {
                // The specific message is shown depending on the exception met
                MessageBox.Show(ex.Message);
                atendee1 = new Atendee();
            }
 
        }

        private void btnGet_Click(object sender, RoutedEventArgs e)
        {
            //Every value is set to the specific value set in the text boxes and buttons
            txtAtendeeRef.Text = atendee1.AtendeeRef.ToString();
            txtFirstName.Text = atendee1.FirstName;
            txtLastName.Text = atendee1.LastName;
            txtConfName.Text = atendee1.ConfName;
            txtPaperTitle.Text = atendee1.PaperTitle;
            CmbRegType.Text = atendee1.RegType;
            if (atendee1.Paid == true)
                radPaidYes.IsChecked = true;
            else
                radPaidNo.IsChecked = true;
            if (atendee1.Presenter == true)
                radPresenterYes.IsChecked = true;
            else
                radPresenterNo.IsChecked = true;
            txtInstName.Text = atendee1.InstName;
        }
        
        private void btnInvoice_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // the atendee1 object is passed to the Invoice window using the constructor built in the Invoice class
                Invoice invoice = new Invoice(atendee1);
                invoice.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please press 'Set' first!");
            }
        }

        private void btnCertificate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                // the atendee1 object is passed to the Certificate window using the constructor built in the Certificate class
                Certificate certificate = new Certificate (atendee1);
                certificate.ShowDialog();
            }
            catch (Exception ex)
            {
                MessageBox.Show("Please press 'Set' first!");
            }
        }


    }
}
