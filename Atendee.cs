﻿/* Author: Alex Dan
   Description : Atendee class is a child class of Person and contains additional details about an atendee with the vlidation required. 
   Last modified : 2016-10-17 */
using System;

namespace SD2CW1
{
    public class Atendee : Person
    {
        //provides the atendee reference number 
        private int atendeeRef;
        //holds conference name
        private string confName;
        // checks if the atendee paid or not
        private bool paid = true;
        //checks if the atendee is a presenter of not
        private bool presenter =true;
        //holds the paper title of the atendee if he is a presenter
        private string paperTitle;
        //shows the registration type of the atendee
        private string regType;
        //shows the institution the atendee belongs to
        private Institution inst = new Institution();

        //allows others to read and set the reference number accordingly
        public int AtendeeRef 
        {
            get
            {
                return atendeeRef;
            }
            set
            {
                if (value < 40000 || value > 60000)
                {
                    throw new ArgumentException("Reference number invalid. Please type in a number between 40000 and 60000");
                }
                atendeeRef = value;
            }
        }
        //allows others to read and set the conference name accordingly
        public string ConfName
        {
            get
            {
                return confName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Please put in a Conference Name");
                }
                confName = value;
            }
        }
        //allows others to read and set if the atendee is a presenter or not
        public bool Presenter
        {
            get
            {
                return presenter;
            }
            set
            {
                presenter = value;
            }
        }
        //allows others to read and set the registration type accordingly
        public string RegType
        {
            get
            {
                return regType;
            }
            set
            {
                if (value != "Full" && value != "Student" && value != "Organiser")
                {
                    throw new ArgumentException("Please select a registration type");
                }
                regType = value;
            }
        }

        //allows others to read and set the paper title accordingly
        public string PaperTitle
        {
            get
            {
                return paperTitle;
            }
            set
            {
                if (presenter)
                {
                    if (value.Trim().Length == 0)
                    {
                        throw new ArgumentException("Please put in a paper title");
                    }
                    paperTitle = value;
                }
                else
                {
                    if (value.Trim().Length != 0)
                        throw new ArgumentException("There cannot be a paper title");
                    paperTitle = value;
                }
            }
        }
        //allows others to read and set if the atendee has paid or not
        public bool Paid
        {
            get { return paid; }
            set { paid = value; }
        }
        //allows others to read and set the institution state accordingly
        public string InstName
        {
            get { return inst.InstitutionName; }
            set { inst.InstitutionName = value; }
        }
        public string InstAddress
        {
            get { return inst.InstitutionAddress;}
            set { inst.InstitutionAddress = value; }
        }
        //gets the cost to be paid by the atendee 
        public double GetCost()
        {
            double toPay = 0;
            switch (regType)
            {
                case "Organiser": toPay = 0; break;
                case "Student": toPay = 300; break;
                case "Full": toPay = 500; break;
            }

            if (presenter)
                 toPay -= 0.1 * toPay;
            
            return toPay;
           
        }
    }
}
