﻿/*Author : Alex Dan
  Description: Person class stores the first and last name of a person
  Last Modified : 2016-10-10*/
using System;

namespace SD2CW1
{
    public class Person
    {
        private string firstName;
        // This method makes sure that the first name is not left blank. If so it will throw an exception. 
        public string FirstName 
        {
            get
            {
                return firstName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Please put in a first name");
                }
                firstName = value;
            }
        }
        private string lastName;
        // This method makes sure that the last name is not left blank. If so it will throw an exception. 
        public string LastName
        {
            get
            {
                return lastName;
            }
            set
            {
                if (value.Length == 0)
                {
                    throw new ArgumentException("Please put in a last name");
                }
                lastName = value;
            }
        }
    }
}