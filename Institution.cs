﻿/* Author : Alex Dan
   Description: This class stores the adress and the name of an institution
   Last modified: "2016-10-17"
*/
using System;

namespace SD2CW1
{
    public class Institution
    {
        private string institutionName;
        private string institutionAddress;

        public string InstitutionName
        {
            get { return institutionName; }
            set { institutionName = value; }
        }
        public string InstitutionAddress
        {
            get { return institutionAddress; }
            set { institutionAddress = value; }
        }
    }
}