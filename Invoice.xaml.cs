﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SD2CW1
{
    /// <summary>
    /// Author: Alex Dan
    /// Displays the price that the antendee has paid or has to pay
    /// Last modified : 2016-10-17
    /// </summary>
    public partial class Invoice : Window
    {
        Atendee atendee1;
        public Invoice(Atendee a)
        {
            InitializeComponent();
            atendee1 = a;
            lblSetName.Content = a.FirstName + " "+ a.LastName;
            lblSetConf.Content = a.ConfName;
            lblSetInst.Content = a.InstName;
            lblSetPaid.Content = a.GetCost();
        }
    }
}
